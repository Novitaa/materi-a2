// console.log('Hello World');
// console.error('This is an error');
// console.warn('This is a warning');

// let, const
// const age = 30;
// age = 31;

// console.log(age);
 
// const score = 10;

// console.log(score);

// DATA TYPES - String, Number, Boolean, null, undefined
// const name = 'Novita';
// const age = 25;
// const rating = 3.5;
// const isCool = true;
// const x = null;
// const y = undefined;
// let z;

// console.log(typeof x);

// DATA TYPES - String, Number, Boolean, null, undefined
// const name = 'Novita';
// const age = 25;

// // Concatenation
// console.log('My name is '+ name + 'and I am' + age);
// // Template String
// const hello = 'My name is ${name} + and I am ${age}';

// console.log(hello);

// const s = 'Hello World!';

// console.log(s.split(''));

// const s = 'technology, computer, it, code';

// console.log(s.split(''));

// Arrays - variables that hold multiple values

// const numbers = new Array(1,2,3,4,5)

// console.log(numbers);

// const fruits = ['apples', 'oranges', 'pears'];

// fruits[3] = 'grapes';

// fruits.push('mangos');

// fruits.unshift('strawberries');

// fruits.pop();

// console.log(Array.isArray('hello'));

// console.log(fruits.indexOf('oranges'));

// console.log(fruits);

// OBJECT LITERALS
// const person = {
//     firstName: 'Novi',
//     age: 25,
//     hobbies: ['music', 'movies', 'cooking'],
//     address: {
//       street: '50 Main st',
//       city: 'Boston',
//       state: 'MA'
//     }
//   }

//   console.log(person.address.city);

// const { firstName, Lastname, address: {city}} = person;

// console.log(city);

// person.email = 'nvita6513@gmail.com';

// console.log(person);

// Array of objects
// const todos = [
//     {
//       id: 1,
//       text: 'Take out trash',
//       isCompleted: false
//     },
//     {
//       id: 2,
//       text: 'Meeting with boss',
//       isCompleted: true
//     },
//     {
//       id: 3,
//       text: 'Destist appt',
//       isCompleted: false
//     }
//   ];

//   const todoJSON = JSON.stringify(todos);
//   console.log(todoJSON);

// For
// for(let i = 0; i <= 10; i++){
//     console.log(`For Loop Number: ${i}`);
//   }

// // While
// let i = 0
// while(i <= 10) {
//   console.log(`While Loop Number: ${i}`);
//   i++;
// }

// for(let i = 0; i < todos.length; i++){
//     console.log(todos[i].text);
//   }

// for(let todo of todos){
//     console.log(todo.id);
//   }

// forEach, map, filter
// todos.map(function(todo) {
//     console.log(todo.text);
//   });

// const todoText = todos.map(function(todo) {
//     return todo.text;
// });

// console.log(todoText);

// const todoCompleted = todos.filter(function(todo) {
//     return todo.isCompleted === true;
// }).map(function(todo) {
//     return todo.text;
// })

// console.log(todoCompleted);

// const x = 6;
// const y = 11;

// if(x > 5 && y > 10) {
//   console.log('x is more than 5 pr y is more than 10');
// } 
// if(x > 5) {
//     if(y > 10) {
//     }
//   console.log('x is greater than 10');
// } else {
//   console.log('x is less than 10')
// }

// const x = 9

// const color = 'green';

// switch(color) {
//   case 'red':
//     console.log('color is red');
//     break;
//   case 'blue':
//     console.log('color is blue');
//     break;
//   default:  
//     console.log('color is not red or blue')
//     break;
// }

// const addNums = num1 => num1 + 5;

// console.log(addNums(5));

// Constructor function
// function person(firstName, Lastname, dob) {
//   this.firstName = firstName;
//   this.Lastname = Lastname;
//   this.dob = new Date(dob);
//   this.getBirthYear = function() {
//     return this.dob.getFullYear();
//   }
//   this.getFullName = function() {
//     return '${this.firstName} ${this.lastName}';
//   }
// }

//   person.prototype.getBirthYear = function() {
//     return this.dob.getFullYear();
//   }

//   person.prototype.getFullName = function() {
//     return '${this.firstName} ${this.lastName}';
//   }

// Class
// class Person {
//     constructor(firstName, lastName, dob) {
//       this.firstName = firstName;
//       this.lastName = lastName;
//       this.dob = new Date(dob);
//     }

//   getBirthYear() {
//     return this.dob.getFullYear();
//   }

//   getFullName() {
//     return `${this.firstName} ${this.lastName}`;
//   }
// }

// // Instantiate object
// const person1 = new Person('John', 'Doe', '4-3-1980');
// const person2 = new Person('Mary', 'Smith', '3-6-1970');

// console.log(person2.getFullName());
// console.log(person1);

// Single Element Selectors
// console.log(document.getElementById('my-form'));
// console.log(document.querySelector('h1'));

// Multiple Element Selectors
// console.log(document.querySelectorAll('.item'));
// console.log(document.getElementsByClassName('item'));
// console.log(document.getElementsByTagName('li'));

// const items = document.querySelectorAll('.item');
// items.forEach((item) => console.log(item));

// const ul = document.querySelector('.items');

// ul.remove();
// ul.lastElementChild.remove();
// ul.firstElementChild.textContent = 'Hello';
// ul.children[1].innerText = 'Novi';
// ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

// const btn = document.querySelector('.btn');
// btn.style.background = 'red';

// const btn = document.querySelector('.btn');

// btn.addEventListener('mouseout', (e) => {
//     e.preventDefault();
//     document.querySelector('#my-form').style.background = '#ccc';
//     document.querySelector('body').classList.add('bg-dark');
//     document.querySelector('.items')
//     .lastElementChild.innerHTML = '<h1>Hello</h1>';
//   });

const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
  e.preventDefault();
  
//   console.log(nameInput.value);

if(nameInput.value === '' || emailInput.value === '') {
    msg.classList.add('error');
    msg.innerHTML = 'Please enter all fields';

    serTimeout(() => msg.remove(), 3000);
  } else {
    const li = document.createElement('li');
    li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));

    userList.appendChild(li);

    nameInput.value = '';
    emailInput.value = '';

  }
}